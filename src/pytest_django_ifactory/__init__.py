"""A model instance factory for pytest-django."""

from .ifactory import InstanceFactory

__all__ = ["InstanceFactory", "__version__"]

__version__ = "1.3.0.dev0"
